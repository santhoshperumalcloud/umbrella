# README #

This README would normally document whatever steps are necessary to get your application up and running and understanding the purpose of the app.

# Languages
* Swift 3.X
* xCode 8.3.3

# Functionality

Show the current and hourly forecasting details based on Zip Code.

Every time that the application becomes the foreground app, the application will fetch the weather. 
If the user has not entered a ZIP code previously, the application will automatically prompt the user for the ZIP code.

User can update ZipCode and Temperature any at point of time and the app will fetch the data. Setting icon will navigate
to ZipCode Entry screen. 

## Zip Code Entry 

The user will be able to enter the ZIP code and choose Temperature unit. 

## Hourly Weather Display

The hourly weather has two main sections. The top section is the current conditions of the entered ZIP code. 
If the temperature is below 60˚, forecast will be displayed in cool color (Blue). If the temperature is 60˚ or above, 
forecast will be displayed in warm color(Orange).

The other section of the main weather display is the hourly forecast. The data from the WeatherApi will be grouped by days. 
The highest temperature of the day will have an warm tint. The lowest temperature of the day will have a cool tint. 
If there is a tie for the high or low, just highlight the first occurrence. 
If there is an occurrence where the high and low are the same hour, no tint color will be applied.

# APIs

This app has been integrated with Weather Underground API to fetch forecasting details.

http://api.wunderground.com/api/{registeredAPIKey}//conditions/hourly/q/{zip}.json

* WeatherApi: This is the Weather Underground API. The only implemented endpoint is /conditions/hourly.
