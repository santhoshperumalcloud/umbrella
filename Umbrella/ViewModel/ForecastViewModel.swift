//
//  ForecastViewModel.swift
//  Umbrella
//
//  Created by Santhosh Perumal on 9/9/17.
//  Copyright © 2017 Santhosh Perumal. All rights reserved.
//

import UIKit

/**
 ForecastViewModel : Class to call REST service and get Weather Forecast details.
 Also convert the responseJSON to the ViewModel object to present on UI.
 */
class ForecastViewModel: NSObject {
    
    // To make REST Service call
    let dataManager = JSONService()
    
    // Current ZipCode City Name
    var currentCityName: String?
    
    // Current ZipCode Forecast temperature in Fahrenheit
    var currentForecastTempFahrenheit: Int?
    
    // Current ZipCode Forecast temperature in Celcius
    var currentForecastTempCelcius: Int?
    
    // Current ZipCode Forecast Weather Status
    var currentWeatherStatus: String?
    
    // Forecast days
    var forecastDays: [String]?
    
    // Hourly Forecast Details. Example value [day:[hour1Forecast1,hour2Forecast,hour3Forecast]]
    var hourlyForecast: [String: [HourlyForecastViewModel]] = [String: [HourlyForecastViewModel]]()
    
    // Store today's hourly forecast details
    var todayHourlyForecast: [HourlyForecastViewModel]?
    
    // Store tomorrow's hourly forecast details
    var tomorrowHourlyForecast: [HourlyForecastViewModel]?
    
    // Store overmorrow's hourly forecast details
    var overmorrowHourlyForecast: [HourlyForecastViewModel]?
    
    // Store overmorrowTitle name
    var overmorrowTitle: String?
    
    // Store JSON data from JSONService
    var jsonData: [String:Any]? {
        didSet {
            if let currentObservation = jsonData?["current_observation"] as? [String:Any] {
                let location = currentObservation["display_location"] as? [String:String]
                self.currentCityName = location?["full"]
                self.currentWeatherStatus = currentObservation["weather"] as? String
                self.currentForecastTempFahrenheit = currentObservation["temp_f"] as? Int
                self.currentForecastTempCelcius = currentObservation["temp_c"] as? Int
            }
            
            var hourlyDetails = [HourlyForecastViewModel]()
            var days = [String]()
            // Read hourlt forcast details
            if let forecast = jsonData?["hourly_forecast"] as? [[String:Any]] {
                for hourlyForecast in forecast {
                    let forcast = HourlyForecastViewModel()
                    let fctTime = hourlyForecast["FCTTIME"] as? [String:Any]
                    forcast.hour = fctTime?["hour"] as? String
                    forcast.day = fctTime?["mday"] as? String
                    forcast.weekdayName = fctTime?["weekday_name"] as? String
                    days.append(forcast.day!)
                    forcast.time = fctTime?["civil"] as? String
                    let temp = hourlyForecast["temp"] as? [String:Any]
                    forcast.forecastTempFahrenheit = temp?["english"] as? String
                    forcast.forecastTempCelcius = temp?["metric"] as? String
                    forcast.weatherStatus = hourlyForecast["condition"] as? String
                    forcast.forecastImage = getForecastWeatherImage(weatherCondition: forcast.weatherStatus!,
                                                                    hour: Int(forcast.hour!)!)
                    hourlyDetails.append(forcast)
                }
            }
            // Remove duplicates days and Get unique days
            forecastDays = days.removeDuplicates()
            var dayCount = 0
            for monthDay in forecastDays! {
                let filteredArray = hourlyDetails.filter({$0.day == monthDay})
                // Filter hourlyForcast based on day and store it into a dictionary
                hourlyForecast[monthDay] = filteredArray
                // Store hourly forecast based on day
                if dayCount == 0 {
                    todayHourlyForecast = filteredArray
                    setHighLowTemperatureFlag(hourlyForecasts: todayHourlyForecast!)
                } else if dayCount == 1 {
                    tomorrowHourlyForecast = filteredArray
                    setHighLowTemperatureFlag(hourlyForecasts: tomorrowHourlyForecast!)
                }else if dayCount == 2 {
                    overmorrowHourlyForecast = filteredArray
                    overmorrowTitle = overmorrowHourlyForecast?.first?.weekdayName
                    setHighLowTemperatureFlag(hourlyForecasts: overmorrowHourlyForecast!)
                }
                dayCount += 1
            }
        }
    }

    
    // Get Weather Forecast Details From REST Service
    func getForecastData(zipCode: String, completionBlock: @escaping ((_ status: String) -> Void)) {
        let url = "http://api.wunderground.com/api/b7944cb2b053859d//conditions/hourly/q/\(zipCode).json"
        dataManager.getData(url: url) { responseJSONData in
            if let json = responseJSONData {
                // Check the any error returning from Remote API
                let response = json["response"] as? [String:Any]
                let error = response?["error"] as? [String:Any]
                if error != nil {
                    completionBlock(error?["description"] as! String)
                } else {
                    self.jsonData = json
                    completionBlock(success)
                }
            } else {
                completionBlock("Unable to fetch the data")
            }
        }
    }
    
    // Filter Hightest and Lowest temp and set the appropirate value
    func setHighLowTemperatureFlag(hourlyForecasts: [HourlyForecastViewModel]){
        let hightestTemp = hourlyForecasts.max { $0.forecastTempFahrenheInt! < $1.forecastTempFahrenheInt! }
        hightestTemp?.isHighest = true
        let lowestTemp = hourlyForecasts.max { $0.forecastTempFahrenheInt! > $1.forecastTempFahrenheInt! }
        lowestTemp?.isLowest = true
    }
    
    // Get forecasting image based on weather condition and hour
    func getForecastWeatherImage(weatherCondition: String, hour: Int) -> String {
        var image = ""
        switch(weatherCondition as String){
        case let s where s.range(of: "Sunny") != nil:
            image = "sunny"
        case let p where p.range(of: "Partly") != nil:
            if hour > 6 && hour < 20 {
                image = "sunny_partlycloudy"
            }else{
                image = "moon_partlycloudy"
            }
        case let p where p.range(of: "Overcast") != nil:
            image = "cloudy"
        case let c where c.range(of: "Clear") != nil:
            if hour > 6 && hour < 20 {
                image = "sunny"
            }else{
                image = "moon"
            }
        case let t where t.range(of: "Thunderstorms") != nil:
            image = "lightning"
        case let s where s.range(of: "Snow") != nil:
            image = "snowy"
        case let f where f.range(of: "Flurries") != nil:
            image = "snowy"
        case let s where s.range(of: "Sleet") != nil:
            image = "snowy"
        case let f where f.range(of: "Freezing Rain") != nil:
            image = "snowy_rainy"
        case let r where r.range(of: "Rain") != nil:
            image = "rainy"
        case let f where f.range(of: "Fog") != nil:
            image = "fog"
        case let f where f.range(of: "Wind") != nil:
            image = "windy_variant"
        case let f where f.range(of: "wind") != nil:
            image = "windy_variant"
        case let m where m.range(of: "Mostly") != nil:
            image = "cloudy"
        case let c where c.range(of: "Cloud") != nil:
            image = "cloudy"
        default:
            image = "cloudy"
        }
        return image
    }
}

// Add a function to remove duplicate elements from an Array
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}
