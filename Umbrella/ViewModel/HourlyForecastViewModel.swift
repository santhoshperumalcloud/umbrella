//
//  HourlyForecastViewModel.swift
//  Umbrella
//
//  Created by Santhosh Perumal on 9/9/17.
//  Copyright © 2017 Santhosh Perumal. All rights reserved.
//

import UIKit

class HourlyForecastViewModel: NSObject {
    
    // Store day of the month
    var day: String?
    
    // Store hour number. Format like 9, 11
    var hour: String?
    
    // Store time. Format like 9 AM, 11 PM
    var time: String?
    
    // Store Weather Status. Example: Sunny, Cloudy
    var weatherStatus: String?
    
    // Store WeekDay Name . Example: Monday
    var weekdayName: String?
    
    // Store Temperature in fahrenheit. Example: 65°
    var forecastTempFahrenheit: String? {
        didSet {
            forecastTempFahrenheInt = Int(self.forecastTempFahrenheit!)
        }
    }
    
    // Store Temperature in Fahrenhe. Example: 65°
    var forecastTempFahrenheInt: Int?
    
    // Store Temperature in Celcius. Example: 65°
    var forecastTempCelcius: String? {
        didSet {
            forecastTempCelciusInt = Int(self.forecastTempCelcius!)
        }
    }
    
    // Store Temperature in Celcius. Example: 25°
    var forecastTempCelciusInt: Int?
    
    // Store true if the current hour temperature is the highest value
    var isHighest: Bool?
    
    // Store true if the current hour temperature is the lowest value
    var isLowest: Bool?
    
    // Store forecast Image
    var forecastImage: String?
    
    // Initialize with default values
    override init() {
        super.init()
        time = ""
        weatherStatus = ""
        forecastTempFahrenheit = "0"
        forecastTempCelcius = "0"
        isLowest = false
        isHighest = false
    }
    
}
