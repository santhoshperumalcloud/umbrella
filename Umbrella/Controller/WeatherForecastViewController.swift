//
//  WeatherForecastViewController.swift
//  Umbrella
//
//  Created by Santhosh Perumal on 9/9/17.
//  Copyright © 2017 Santhosh Perumal. All rights reserved.
//

import UIKit

// Constants
let warmColor = UIColor.init(netHex:0xff9800)
let coolColor = UIColor.init(netHex:0x03a9f4)
let degree = "\u{00B0}"
let zipCodeVC = "ZipCodeVC"
let success = "Success"
let forecastHeaderIdentifier = "ForecastHeader"
let forecastCellIdentifier = "ForecastCell"
let loading = "Loading..."

/**
 WeatherForecastViewController: Show current forecast details and hourly forecast details for next 36 hours
 based on the stored ZipCode.
 */
class WeatherForecastViewController: UIViewController {
    
    // To display ZipCode city name
    @IBOutlet weak var currentCity: UILabel!
    
     // To display current temperature (fahrenheit or celcius) on top view section
    @IBOutlet weak var currentTemperature: UILabel!
    
    // To display current weather status on top view section
    @IBOutlet weak var currentWeatherStatus: UILabel!
    
    // Setting button to navigate to ZipCodeViewController
    @IBOutlet weak var settingButton: UIButton! {
        didSet {
            let image = UIImage(named: "settings")?.withRenderingMode(.alwaysTemplate)
            settingButton.setImage(image, for: .normal)
            settingButton.tintColor = UIColor.white
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    // UIView to display current forecasting details on top of the screen
    @IBOutlet weak var currentForecastTempUIView: UIView! {
        didSet {
            // Setup shadow attributes
            currentForecastTempUIView.layer.masksToBounds = false
            currentForecastTempUIView.layer.shadowOffset = CGSize(width: 0, height: 0)
            currentForecastTempUIView.layer.shadowColor = UIColor.gray.cgColor
            currentForecastTempUIView.layer.shadowOpacity = 1
        }
    }
    
    // To show  today's hourly forecasting details
    @IBOutlet weak var todayForecastCollectionView: UICollectionView! {
        didSet {
            todayForecastCollectionView.delegate = self
            todayForecastCollectionView.dataSource = self
        }
    }
    
    // To show  tomorrow's hourly forecasting details
    @IBOutlet weak var tomorrowForecastCollectionView: UICollectionView! {
        didSet {
            tomorrowForecastCollectionView.delegate = self
            tomorrowForecastCollectionView.dataSource = self
        }
    }
    
    // To show  overmorrow's hourly forecasting details
    @IBOutlet weak var overmorrowForecastCollectionView: UICollectionView! {
        didSet {
            overmorrowForecastCollectionView.delegate = self
            overmorrowForecastCollectionView.dataSource = self
        }
    }
    
    // Store UICollectionView height constraint. Will be modified based on number forecasting cells.
    @IBOutlet weak var todayCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tomorrowCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var overmorrowCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Instance of ViewModel to fetch forecasting details from Remote API
    let forecastViewModel = ForecastViewModel()
    
    // Constant: UICollectionView Section Header height
    let sectionHeaderHeight: Float  = 60.0
    
    // Constant: UICollectionView Height to calculate cells per row
    let collectionViewCellHeight: Float = 95.0
    
    // Store user entered Zip Code to fetch forecasting details
    var zipCodeValue: String?
    
    // Store user entered temperature unit (fahrenheit or celcius)
    var temperatureUnit: String?
    
    // Store Forecast Day to set on Segment Title.
    var overmorrowTitle: String?
    
    // Indicator to find data loading state
    var isLoadingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configure Today and Tomorrow CollectionView Controller's
        configureCollection(collectionView: todayForecastCollectionView)
        configureCollection(collectionView: tomorrowForecastCollectionView)
        configureCollection(collectionView: overmorrowForecastCollectionView)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(WeatherForecastViewController.getForecastData)
            , name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    // Every time that the application becomes the foreground app, the application should fetch the weather.
    override func viewDidAppear(_ animated: Bool) {
        getForecastData()
    }
    
    /* Get forecasting details from Remote API. This function will also get notified
       when the app did become action in foreground */
    func getForecastData(){
        guard !isLoadingData else {
            // Data loading is already in process
            return
        }
        // Get ZipCode from UserDefaults. If nil, redirect to ZipCodeViewController
        zipCodeValue = UserDefaults.standard.value(forKey: zipCodeKey) as? String
        temperatureUnit = UserDefaults.standard.value(forKey: temperatureUnitKey) as? String
        /* If the user has not entered a ZIP code  or Temperatire Unit are not set previously,
           the application should automatically refirect to ZipCodeViewController screen to get the values  */
        if zipCodeValue == nil || temperatureUnit == nil {
            performSegue(withIdentifier: zipCodeVC, sender: self)
        } else {
            scrollView.setContentOffset(CGPoint.zero, animated: false)
            isLoadingData = true
            showActivityIndicatorView()
            forecastViewModel.getForecastData(zipCode: zipCodeValue!) { status in
                guard status == success else {
                    self.isLoadingData = false
                    DispatchQueue.main.async {
                        self.dismissActivityIndicatorView()
                        // Show Error message if unable to fetch the forecasting data
                        let alertController = UIAlertController(title: "Error",
                                    message: status,
                                    preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .default,
                                                          handler: {( alert: UIAlertAction!) in
                                                            // Redirect to zipcode UI
                                                            if status.range(of: "No cities match") != nil {
                                                                self.performSegue(withIdentifier: zipCodeVC,
                                                                                  sender: self)
                                                            }
                                                          })
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    return
                }
                DispatchQueue.main.async {
                    // Update UI with main queue
                    if let cityName = self.forecastViewModel.currentCityName {
                        self.currentCity.text = cityName
                    }
                    if let forecastTemp = self.forecastViewModel.currentForecastTempFahrenheit {
                        // Set current temperature based on unit
                        switch self.temperatureUnit! {
                        case fahrenheit:
                            self.currentTemperature.text = String(forecastTemp) + degree
                        case celcius:
                            self.currentTemperature.text = String(self.forecastViewModel.currentForecastTempCelcius!)
                                                            + degree
                        default:
                            break
                        }
                        if forecastTemp > 60 {
                            // Set Warm Color if the temp is above 60°
                            self.currentForecastTempUIView.backgroundColor = warmColor
                        } else {
                            // Set Cool Color if the temp is below 60°
                            self.currentForecastTempUIView.backgroundColor = coolColor
                        }
                        
                    }
                    if let weather = self.forecastViewModel.currentWeatherStatus {
                        self.currentWeatherStatus.text = weather
                    }
                    // Reload hourly forecast details
                    self.reloadCollectionViewData()
                    self.isLoadingData = false
                    self.dismissActivityIndicatorView()
                }
            }
            
        }
    }
    
    // Reload hourly forecast details and setup scroll contentSize
    func reloadCollectionViewData() {
        todayForecastCollectionView.reloadData()
        tomorrowForecastCollectionView.reloadData()
        overmorrowForecastCollectionView.reloadData()
        var padding = CGFloat(48)
        // Hide overmorrowForecastCollectionView if there is no cell to display
        if forecastViewModel.overmorrowHourlyForecast == nil || forecastViewModel.overmorrowHourlyForecast?.count == 0 {
            overmorrowCollectionHeight.constant = 0
            overmorrowForecastCollectionView.isHidden = true
            padding = CGFloat(36)
        } else {
            overmorrowForecastCollectionView.isHidden = false
        }
        let contentHeight = todayCollectionHeight.constant + tomorrowCollectionHeight.constant
                                                           +  overmorrowCollectionHeight.constant + padding
        scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: contentHeight)
        scrollView.setNeedsLayout()
        scrollView.layoutIfNeeded()
    }
    
    // Configure Shadow attributes for UICollectionViewController
    func configureCollection(collectionView: UICollectionView){
        collectionView.layer.masksToBounds = false
        collectionView.layer.cornerRadius = 2.0
        collectionView.layer.shadowOffset = CGSize(width: 0, height: 0)
        collectionView.layer.shadowColor = UIColor.gray.cgColor
        collectionView.layer.shadowOpacity = 1
    }
    
    // Seque to navigate ZipViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if zipCodeVC == segue.identifier {
            if let destinationVC = (sender as AnyObject).destination as? ZipCodeViewController {
                self.present(destinationVC, animated: true, completion: nil)
            }
        }
    }
    
    // Show activity loading cursor when loading data from Remote API
    func showActivityIndicatorView(){
        activityIndicator.startAnimating()
        self.view.alpha = 0.8
    }
    
    // Dismiss activity loading cursor
    func dismissActivityIndicatorView() {
        activityIndicator.stopAnimating()
        self.view.alpha = 1.0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension WeatherForecastViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                         withReuseIdentifier: forecastHeaderIdentifier,
                                                                         for: indexPath) as! WeatherForecastHeaderView
            // Set Section Title with day name
            if collectionView == self.overmorrowForecastCollectionView {
                header.forecastDay.text = self.forecastViewModel.overmorrowTitle
            }
            return header
        default:
            assert(false,"Expected UICollectionElementKindSectionHeader kind. Received unexpected element kind. ")
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // Each forecast day will have only one section
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // Calculate number of cells in each collection view and height
        if collectionView == self.todayForecastCollectionView {
             if let noOfItems = forecastViewModel.todayHourlyForecast?.count {
                todayCollectionHeight.constant = (CGFloat(sectionHeaderHeight
                                                + (collectionViewCellHeight * Float(ceil(Float(noOfItems)/4)))))
                return noOfItems
            }
        } else if collectionView == self.tomorrowForecastCollectionView {
            if let noOfItems = forecastViewModel.tomorrowHourlyForecast?.count {
                tomorrowCollectionHeight.constant = (CGFloat(sectionHeaderHeight
                    + (collectionViewCellHeight * Float(ceil(Float(noOfItems)/4)))))
                return noOfItems
            }
        } else if collectionView == self.overmorrowForecastCollectionView {
            if let noOfItems = forecastViewModel.overmorrowHourlyForecast?.count {
                overmorrowCollectionHeight.constant = (CGFloat(sectionHeaderHeight
                    + (collectionViewCellHeight * Float(ceil(Float(noOfItems)/4)))))
                return noOfItems
            }
        }
        // return default value as 0
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let forecastCell = collectionView.dequeueReusableCell(withReuseIdentifier: forecastCellIdentifier,
                                                              for: indexPath) as? WeatherForecastCell
        // Update hourly forecast details for each day
        var hourDetail: HourlyForecastViewModel?
        if collectionView == self.todayForecastCollectionView {
            hourDetail = (forecastViewModel.todayHourlyForecast?[indexPath.row])!
        } else if collectionView == self.tomorrowForecastCollectionView {
            hourDetail = (forecastViewModel.tomorrowHourlyForecast?[indexPath.row])!
        } else if collectionView == self.overmorrowForecastCollectionView {
            hourDetail = (forecastViewModel.overmorrowHourlyForecast?[indexPath.row])!
        }
        forecastCell?.hour.text = hourDetail?.time
        // Display weather temperature based on unit set by user
        switch temperatureUnit! {
        case fahrenheit:
             forecastCell?.weatherTemp.text = (hourDetail?.forecastTempFahrenheit)! + degree
        case celcius:
             forecastCell?.weatherTemp.text = (hourDetail?.forecastTempCelcius)! + degree
        default:
            break
        }
        
        // Set Forecasting image based on weather condition status
        let image = UIImage(named: (hourDetail?.forecastImage)!)?.withRenderingMode(.alwaysTemplate)
        forecastCell?.forecastImage.setImage(image, for: .normal)
        
        if(hourDetail?.isLowest)!{
            // Set the lowest temperature of the day should have a cool tint
            forecastCell?.forecastImage.tintColor = coolColor
            forecastCell?.hour.textColor = coolColor
            forecastCell?.weatherTemp.textColor = coolColor
        } else if (hourDetail?.isHighest)! {
            // Set the highest temperature of the day should have an warm tint
            forecastCell?.forecastImage.tintColor = warmColor
            forecastCell?.hour.textColor = warmColor
            forecastCell?.weatherTemp.textColor = warmColor
        } else {
            // Set default color to black
            forecastCell?.forecastImage.tintColor = UIColor.black
            forecastCell?.hour.textColor = UIColor.black
            forecastCell?.weatherTemp.textColor = UIColor.black
        }
        //  If there is an occurrence where the high and low are the same hour, do not use a tint color.
        if (hourDetail?.isLowest)! && (hourDetail?.isHighest)! {
            // Set default color to black
            forecastCell?.forecastImage.tintColor = UIColor.black
            forecastCell?.hour.textColor = UIColor.black
            forecastCell?.weatherTemp.textColor = UIColor.black
        }
        
        return forecastCell!
    }
    
}

// Custom UICollectionView Section Header to show forecasting Day
class WeatherForecastHeaderView: UICollectionReusableView {
    // To Show Section Header title
    @IBOutlet weak var forecastDay: UILabel!
    
}

// Custom UICollectionViewCell to show hourly forecasting
class WeatherForecastCell: UICollectionViewCell {
    
    @IBOutlet weak var hour: UILabel!
    
    @IBOutlet weak var forecastImage: UIButton!
    
    @IBOutlet weak var weatherTemp: UILabel!
    
}

// Modify UICollectionCell Space and Width/Heigh based on Device Screen Width to show 4 cells per row.
extension WeatherForecastViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        // Adjust Space between the cell based on device screen width
        if(UIScreen.main.bounds.size.width >= 414){
            // iPhone 7S & 7S
            return 8.0

        }else if (UIScreen.main.bounds.size.width >= 375){
            // iPhone 6 & 6+
            return 8.0
        }else {
            // iPhone 5 & 5+
            return 4.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        // Adjust cell size to show max 4 cells in a row based on device screen width
        if(UIScreen.main.bounds.size.width >= 414){
            // iPhone 7S or 6S+
            return CGSize(width: 80, height: 80)
        }else if (UIScreen.main.bounds.size.width >= 375){
            // iPhone 7 & 6+
            return CGSize(width: 70, height: 80)
        }else {
            // iPhone 5 & 5+. Width - 320
            return CGSize(width: 60, height: 80)
        }
    }
    
}

// extend UIColor to support Hexadecimal color code
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid red component")
        assert(blue >= 0 && blue <= 255, "Invalid red component")
        self.init(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
    }
    // Support Hexadecimal color code
    convenience init(netHex: Int) {
        self.init(red: (netHex >> 16) & 0xff, green: (netHex >> 8) & 0xff, blue: netHex & 0xff)
    }
}

