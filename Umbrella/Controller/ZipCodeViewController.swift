//
//  ZipCodeViewController.swift
//  Umbrella
//
//  Created by Santhosh Perumal on 9/9/17.
//  Copyright © 2017 Santhosh Perumal. All rights reserved.
//

import UIKit

let zipCodeKey = "ZipCode"
let temperatureUnitKey = "TemperatureUnit"
let celcius = "Celcius"
let fahrenheit = "fahrenheit"
let blueColor = UIColor.init(netHex: 0x016EFF)

/**
 ZipCodeViewController : Get ZipCode from user and store it in UserDefaults for Backend API Calls.
 */
class ZipCodeViewController: UIViewController, UITextFieldDelegate {
    
    // Configure updateButton border and State Colors
    @IBOutlet weak var updateButton: UIButton! {
        didSet {
            updateButton.layer.borderColor = UIColor.lightGray.cgColor
            updateButton.layer.borderWidth = 1
            updateButton.layer.cornerRadius = 12
            updateButton.setTitleColor(UIColor.lightGray, for: .disabled)
            updateButton.setTitleColor(blueColor, for: .normal)
            updateButton.isEnabled = false
        }
    }
    // Keyboard customization
    let doneToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:44))
    
    // Configure zipCodeTextField State Colors and Enable if value available in UserDefaults
    @IBOutlet weak var zipCodeTextField: UITextField! {
        didSet {
            zipCodeTextField.delegate = self
            if UserDefaults.standard.value(forKey: zipCodeKey) != nil {
                zipCodeTextField.text = (UserDefaults.standard.value(forKey: zipCodeKey) as! String)
                // Enable updateButton
                updateButton.layer.borderColor = blueColor.cgColor
                updateButton.isEnabled = true
            }
        }
    }
    
    @IBOutlet weak var temperatureUnit: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //dd UIToolBar on keyboard and Done button on UIToolBar
        self.addDoneButtonOnKeyboard()
        
        zipCodeTextField.becomeFirstResponder()
        if let unit = UserDefaults.standard.value(forKey: temperatureUnitKey) as? String {
            switch unit {
            case fahrenheit:
                temperatureUnit.selectedSegmentIndex = 0
            case celcius:
                temperatureUnit.selectedSegmentIndex = 1
            default:
                temperatureUnit.selectedSegmentIndex = 0
            }
        }
    }

    // MARK: - User Action method
    @IBAction func updateButtonAction(_ sender: UIButton) {
        // Store the ZipCode in UserDefaults
        let zipcode = zipCodeTextField.text
        UserDefaults.standard.setValue(zipcode, forKey: zipCodeKey)
        
        switch temperatureUnit.selectedSegmentIndex {
        case 0:
            UserDefaults.standard.setValue(fahrenheit, forKey: temperatureUnitKey)
        case 1:
            UserDefaults.standard.setValue(celcius, forKey: temperatureUnitKey)
        default:
            UserDefaults.standard.setValue(fahrenheit, forKey: temperatureUnitKey)
        }
        zipCodeTextField.resignFirstResponder()
        self.dismiss(animated: true) { }
    }
    
    
    
    // MARK: - UITextFieldDelegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
       
        guard let textValue = textField.text else {
            return false
        }
        var effectiveString = textValue.substring(to: textValue.index(textValue.startIndex,
                                                                offsetBy: textValue.characters.count - range.length))
        // As new character is not added yet to the textfield, therefore appending to effectiveString
        effectiveString = effectiveString.appending(string)
        // Enable Update Button if the ZipCode has 5 digits
        if effectiveString.characters.count >= 5 {
            updateButton.layer.borderColor = blueColor.cgColor
            updateButton.isEnabled = true
            if effectiveString.characters.count > 5 {
                return false
            }
        } else {
            updateButton.layer.borderColor = UIColor.lightGray.cgColor
            updateButton.isEnabled = false
        }
        return true
    }
    
    // Customize Keyboard with Done button
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = .black
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done,
                                                    target: self,
                                                    action: #selector(ZipCodeViewController.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.zipCodeTextField.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        self.zipCodeTextField.resignFirstResponder()
    }
}
