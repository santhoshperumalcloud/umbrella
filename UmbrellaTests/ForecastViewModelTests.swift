//
//  UmbrellaTests.swift
//  UmbrellaTests
//
//  Created by Santhosh Perumal on 9/9/17.
//  Copyright © 2017 Santhosh Perumal. All rights reserved.
//

import XCTest
@testable import Umbrella

class ForecastViewModelTests: XCTestCase {
    
    var forecastViewModel = ForecastViewModel()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetForecastValidData() {
        // Test Asyn Call using XCTestExpectation
        let expect = expectation(description: "Expected Forecasting JSON Data")
        forecastViewModel.getForecastData(zipCode: "94583") { (status) in
            XCTAssertTrue(status == "Success")
            XCTAssertNotNil(self.forecastViewModel.jsonData)
            XCTAssertNotNil(self.forecastViewModel.currentForecastTempFahrenheit)
            XCTAssertNotNil(self.forecastViewModel.currentCityName)
            XCTAssertTrue((self.forecastViewModel.todayHourlyForecast?.count)! > 0)
            XCTAssertTrue((self.forecastViewModel.tomorrowHourlyForecast?.count)! > 0)
            expect.fulfill()
        }
        waitForExpectations(timeout: 2.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetForecastInvalidData() {
        // Test Asyn Call using XCTestExpectation
        let expect = expectation(description: "Expected Forecasting JSON Data")
        forecastViewModel.getForecastData(zipCode: "99999") { (status) in
            XCTAssertTrue(status != "Success")
            XCTAssertNil(self.forecastViewModel.jsonData)
            expect.fulfill()
        }
        waitForExpectations(timeout: 2.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    
    func testGetForecastWeatherImage() {
        var image = forecastViewModel.getForecastWeatherImage(weatherCondition: "Partly Cloudy", hour: 2)
        XCTAssertEqual(image,"moon_partlycloudy")
        image = forecastViewModel.getForecastWeatherImage(weatherCondition: "Partly Cloudy", hour: 8)
        XCTAssertEqual(image,"sunny_partlycloudy")
        image = forecastViewModel.getForecastWeatherImage(weatherCondition: "Thunderstorms", hour: 8)
        XCTAssertEqual(image,"lightning")
        image = forecastViewModel.getForecastWeatherImage(weatherCondition: "Mostly  Cloudy", hour: 8)
        XCTAssertEqual(image,"cloudy")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
